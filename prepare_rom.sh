#!/bin/bash

# Copyright (C) 2012 by MIUIPortugal
# Copyright (C) 2012 by rtfpessoa

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

BASE=$PWD
VERSION=$1

chmod -R a+x *

export PATH=${BASE}/tools:$PATH

# REMOVE OLD FILES #
rm -rf miuirom
rm -rf cmrom
rm -rf miui_I9000*.zip
rm -rf flashable.zip
rm -rf flashable.zip-Toggle2G-resigned.zip

MIUIROM=`ls ${BASE}/miui_crespo_rom/*.zip`
CMROM=`ls ${BASE}/cm10_i9000_rom/*.zip`

# UNZIP NEEDED ROMS #
if [ "$MIUIROM" != "" ]; then
    unzip $MIUIROM -d miuirom > /dev/null
else
    echo "Cannot find MIUI Crespo Rom. Put it under \"miui_crespo_rom\" folder"
    exit 0
fi

if [ "$CMROM" != "" ]; then
    unzip $CMROM -d cmrom > /dev/null > /dev/null
else
    echo "Cannot find CM10 Rom. Put it under \"cm10_i9000_rom\" folder"
    exit 0
fi
   
# FRAMEWORK.JAR #
cd ${BASE}/miuirom/system/framework
apktool150 d framework.jar
cp -rf ${BASE}/patches/framework.jar.out/* ${BASE}/miuirom/system/framework/framework.jar.out/
apktool150 b framework.jar.out
cd framework.jar.out/build/apk
zip -r ${BASE}/miuirom/system/framework/framework.jar classes.dex
rm -rf ${BASE}/miuirom/system/framework/framework.jar.out

# SERVICES.JAR #
cd ${BASE}/miuirom/system/framework
apktool150 d services.jar
cp -rf ${BASE}/patches/services.jar.out/* ${BASE}/miuirom/system/framework/services.jar.out/
apktool150 b services.jar.out
cd services.jar.out/build/apk
zip -r ${BASE}/miuirom/system/framework/services.jar classes.dex
rm -rf ${BASE}/miuirom/system/framework/services.jar.out
cd ${BASE}/miuirom/system/framework        

# ANDROID.POLICY.JAR #
apktool150 d android.policy.jar
cp -rf ${BASE}/patches/android.policy.jar.out/* ${BASE}/miuirom/system/framework/android.policy.jar.out/
cd ${BASE}/miuirom/system/framework
apktool150 b android.policy.jar.out
cd android.policy.jar.out/build/apk
zip -r ${BASE}/miuirom/system/framework/android.policy.jar classes.dex
rm -rf ${BASE}/miuirom/system/framework/android.policy.jar.out
cd ${BASE}/miuirom/system/framework

# FRAMEWORK-RES.APK #
apktool150 d framework-res.apk
cp -rf ${BASE}/patches/framework-res/* framework-res/            
apktool150 b framework-res
cd framework-res/build/apk
zip -r ${BASE}/miuirom/system/framework/framework-res.apk res/xml/storage_list.xml
cd ${BASE}/miuirom/system/framework
rm -rf ${BASE}/miuirom/system/framework/framework-res

# REMOVE UNEEDED FILES #
rm -f ${BASE}/miuirom/system/vendor/lib/libsec-ril.so
rm -f ${BASE}/miuirom/system/vendor/lib/libakm.so
rm -f ${BASE}/miuirom/system/vendor/firmware/libpn544_fw.so
rm -f ${BASE}/miuirom/system/etc/permissions/android.hardware.nfc.xml
rm -f ${BASE}/miuirom/system/etc/permissions/com.android.nfc_extras.xml
rm -f ${BASE}/miuirom/system/etc/nfcee_access.xml		
rm -f ${BASE}/miuirom/system/lib/hw/audio.primary.herring.so
rm -f ${BASE}/miuirom/system/lib/hw/audio_policy.herring.so
rm -f ${BASE}/miuirom/system/lib/hw/camera.herring.so
rm -f ${BASE}/miuirom/system/lib/hw/nfc.herring.so
rm -f ${BASE}/miuirom/system/lib/hw/sensors.herring.so    
rm -f ${BASE}/miuirom/system/app/SuperMarket.apk
rm -f ${BASE}/miuirom/system/app/Nfc.apk
rm -rf ${BASE}/miuirom/recovery
rm -rf ${BASE}/miuirom/META-INF
rm -rf ${BASE}/miuirom/system/modules

# REMOVE LegacyCamera & MiuiGalery # 
rm -f ${BASE}/miuirom/system/app/LegacyCamera.apk
rm -f ${BASE}/miuirom/system/app/MiuiGallery.apk
rm -f ${BASE}/miuirom/system/app/MiuiVideoPlayer.apk

# COPY CM10 NEEDED FILES #
cp -rf ${BASE}/cmrom/META-INF ${BASE}/miuirom/
cp -f ${BASE}/cmrom/bml_over_mtd ${BASE}/miuirom/
cp -f ${BASE}/cmrom/bml_over_mtd.sh ${BASE}/miuirom/
cp -f ${BASE}/cmrom/busybox ${BASE}/miuirom/
cp -f ${BASE}/cmrom/erase_image ${BASE}/miuirom/
cp -f ${BASE}/cmrom/flash_image ${BASE}/miuirom/
cp -f ${BASE}/cmrom/make_ext4fs ${BASE}/miuirom/
cp -f ${BASE}/cmrom/modem.bin ${BASE}/miuirom/
cp -f ${BASE}/cmrom/updater.sh ${BASE}/miuirom/
cp -f ${BASE}/cmrom/system/app/AriesParts.apk ${BASE}/miuirom/system/app/
cp -f ${BASE}/cmrom/system/app/SamsungServiceMode.apk ${BASE}/miuirom/system/app/
cp -f ${BASE}/cmrom/system/bin/make_ext4fs ${BASE}/miuirom/system/bin/
cp -f ${BASE}/cmrom/system/bin/rild ${BASE}/miuirom/system/bin/
cp -f ${BASE}/cmrom/system/bin/tvouthack ${BASE}/miuirom/system/bin/
cp -f ${BASE}/cmrom/system/bin/tvoutserver ${BASE}/miuirom/system/bin/
cp -f ${BASE}/cmrom/system/bin/vold ${BASE}/miuirom/system/bin/
cp -f ${BASE}/cmrom/system/bin/wpa_cli ${BASE}/miuirom/system/bin/
cp -f ${BASE}/cmrom/system/bin/wpa_supplicant ${BASE}/miuirom/system/bin/
cp -f ${BASE}/cmrom/system/etc/vold.conf ${BASE}/miuirom/system/etc/
cp -f ${BASE}/cmrom/system/etc/vold.fstab ${BASE}/miuirom/system/etc/
cp -rf ${BASE}/cmrom/system/lib/hw/* ${BASE}/miuirom/system/lib/hw/
cp -rf ${BASE}/cmrom/system/lib/soundfx/* ${BASE}/miuirom/system/lib/soundfx/
cp -f ${BASE}/cmrom/system/lib/lib_tvoutengine.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libjni_mosaic.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libncurses.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libreference-ril.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libril.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libsec-ril.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libsecril-client.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libskia.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libtvout.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libtvout_jni.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libtvoutfimc.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libtvouthdmi.so ${BASE}/miuirom/system/lib/
cp -f ${BASE}/cmrom/system/lib/libtvoutservice.so ${BASE}/miuirom/system/lib/
cp -rf ${BASE}/cmrom/system/usr/* ${BASE}/miuirom/system/usr/
cp -rf ${BASE}/cmrom/system/vendor/* ${BASE}/miuirom/system/vendor/

# AriesParts.APK #
cd ${BASE}/miuirom/system/app
apktool150 d AriesParts.apk
REPLACEABLE="<activity android:label=\"@string\/app_name\" android:name=\".DeviceSettings\" \/>"
REPLACER="<activity android:label=\"@string\/app_name\" android:name=\".DeviceSettings\">   \
            <intent-filter>                                                                 \
                <action android:name=\"android.intent.action.MAIN\" \/>                     \
                <category android:name=\"android.intent.category.LAUNCHER\" \/>             \
            <\/intent-filter>                                                               \
          <\/activity>"
sed -i "s/${REPLACEABLE}/${REPLACER}/g" ${BASE}/miuirom/system/app/AriesParts/AndroidManifest.xml
apktool150 b AriesParts
cd AriesParts/build/apk
zip -r ${BASE}/miuirom/system/app/AriesParts.apk resources.arsc
zip -r ${BASE}/miuirom/system/app/AriesParts.apk AndroidManifest.xml
cd ${BASE}/miuirom/system/app
signapk AriesParts.apk AriesParts_signed.apk
mv AriesParts_signed.apk AriesParts.apk
rm -rf ${BASE}/miuirom/system/app/AriesParts

# COPY PATCHED FILES #
cp -rf ${BASE}/overlay/* ${BASE}/miuirom/

# COPY KERNEL FILES #
cp -rf ${BASE}/kernel/* ${BASE}/miuirom/

# COPY GAPPS #
rm -f ${BASE}/miuirom/system/app/QuickSearchBox.apk
rm -f ${BASE}/miuirom/system/app/SetupWizard.apk
rm -f ${BASE}/miuirom/system/app/Velvet.apk
rm -f ${BASE}/miuirom/system/app/Vending.apk        
cp -rf ${BASE}/gapps/* ${BASE}/miuirom/

# PACK FINAL ROM #
cd ${BASE}/miuirom
zip -qr flashable.zip *
ResignUpdate flashable.zip > /dev/null
mv flashable.zip-Toggle2G-resigned.zip flashable.zip > /dev/null
zip flashable.zip -d system/app/Toggle2G.apk > /dev/null

if [ "${VERSION}" != "" ]; then
    mv flashable.zip ${BASE}/miui_I9000_${VERSION}_AIO_Signed.zip
else
    mv flashable.zip ${BASE}/miui_I9000_AIO_Signed.zip
fi

# REMOVE UNEEDED FILES #
cd ${BASE}
rm -rf miuirom
rm -rf cmrom
rm -rf flashable.zip
rm -rf flashable.zip-Toggle2G-resigned.zip

echo "DONE !!!"
